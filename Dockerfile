FROM ubuntu

RUN apt-get update && \
    apt-get install -y software-properties-common apt-transport-https ca-certificates curl gnupg2 unzip && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -  && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
    apt-get update  && \
    apt-get install -y docker-ce && \
    apt-get install -y python3-dev python3-pip python3-virtualenv && \
    pip install pylint  && \
    curl https://sdk.cloud.google.com | bash && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl   && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    /bin/bash get_helm.sh


ENV PATH=${PATH}:/root/google-cloud-sdk/bin

EXPOSE 8080
