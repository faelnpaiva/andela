# Andela


- GitLab CI will execute pylint inside helloworld folder , compile the dockerfile inside app folder and push to a registry the docker image with a tag defined with built-in variable CI_PIPELINE_ID .
- Deployment with helm chart won't work with gitlabCI because I haven't a free cluster anymore in gcp nor in azure neither in aws . I tested the helm chart in minikube . CI/CD configured to GCP . 
- To helm chart ingress I'm using a non existent dns/recordset. To execute with minikube it's necessary setup the ip in etc/hosts. documentation ~> https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/  
- ALLOWED_HOSTS changed in helloworld/settings.py to allow pod and host ips . Pod ip is necessary for health-check works and host ip to allow reach the application from ingress address ( using minikube need to check the external URL provided `minikube service list` )
- ALLOW_POD_IP and ALLOW_HOST env vars are injected by helm chart deployment.
- Dockerfile in root path was used to create a custom "agent" to gitlabci.